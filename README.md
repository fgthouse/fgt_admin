fgt_admin
=========

###### Ideas
1. Target players based on:
  * SteamID
  * Ent Index?
  * Ranks/Teams/Privileges/ect. (#admins+#vip-derpy)
2. Convert unicode characters to a qwerty keyboard equivilant.
  * Convert ɐ=a ʟ=L ɔ=c   ect.
3. Kick players with fast changing names.
  * Or implement a system to target these users.
4. Per user privileges, granted or revoked.
5. Redo privilege system.
