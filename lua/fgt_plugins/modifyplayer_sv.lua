FGTPLUGIN.name     = "Modify Player"
FGTPLUGIN.desc     = "Modify a player's privileges." -- TODO: Modify power level and tools/sents/sweps...
FGTPLUGIN.helptext = "<player> <addpriv/rmpriv> <privilege>"
FGTPLUGIN.commands = { "modplayer" }
FGTPLUGIN.privs    = { ["modifyplayer"] = fgtadmin.ranks.superadmin } -- This will give the default ranks the privileges to use certain plugin 'functions'. See ban_sv.lua for a better example.

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "modifyplayer" ) then -- You always should check if your callingPlayer has the privilege to use this command.
		fgtadmin:Warn( callingPly, "You do not have the modifyplayer privilege for this command!")
		return
	end

	if #args < 4 then -- Check if the callingPlayer has given enough arguments.
		fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
		return
	end

	local searchPlayer = args[2]
	local modifyType = args[3]
	local modifyValue = args[4]

	local targetPlayer = fgtadmin:FindPlayersByName( searchPlayer, true )
	if not targetPlayer then
		fgtadmin:Error( callingPly, "No players found." )
		return
	elseif targetPlayer == -1 then
		fgtadmin:Error( callingPly, "There are multiple matches for the player " .. searchPlayer .. ". Please be more specific." )
		return
	else
		if not fgtadmin:PlayerCanTarget( callingPly, targetPlayer ) then
			fgtadmin:Warn( callingPly, "You can't modify a player with an equal or higher power level than you!" )
			return
		else
			if modifyType == "addpriv" then
				fgtadmin:PlayerAddPrivilege( targetPlayer, modifyValue )
				local formattedString = "# gave # the # privilege."
				local adminNick = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick()
				local actionVars = { {adminNick,Color(0,255,0)}, {targetPlayer:Nick(),Color(0,0,255)}, modifyValue }
				fgtadmin:Action( callingPly, fgtadmin:FormatAction(formattedString, actionVars), silentCall )

			elseif modifyType == "rmpriv" then
				fgtadmin:PlayerRemovePrivilege( targetPlayer, modifyValue )
				local formattedString = "# denied #'s # privilege."
				local adminNick = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick()
				local actionVars = { {adminNick,Color(0,255,0)}, {targetPlayer:Nick(),Color(0,0,255)}, modifyValue }
				fgtadmin:Action( callingPly, fgtadmin:FormatAction(formattedString, actionVars), silentCall )

			else
				fgtadmin:Error( callingPly, modifyType .. " isn't a valid argument.\nUsage: " .. self.helptext )
				return
			end
		end
	end
end
