-- _cl plugins dont get run on the server. Consider them a fire once script.

net.Receive( "FgtAdmin_ChatNotification", function( len, client )
	local isSilent = net.ReadBit() -- 0 or 1 not a boolean ...
	local content = net.ReadTable()

	if isSilent == 1 then
		chat.AddText( "(Silent) ", unpack(content) )
	else
		chat.AddText( unpack(content) )
	end
end )

net.Receive( "FgtAdmin_ConsolePrint", function( len, client )
	print( net.ReadString() )
end )
