FGTPLUGIN.name     = "Installed Plugins"
FGTPLUGIN.desc     = "Print out a list of plugins installed on the server."
FGTPLUGIN.helptext = ""
FGTPLUGIN.commands = { "plugins" }
FGTPLUGIN.privs    = { ["plugins"] = fgtadmin.ranks.admin }

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "plugins" ) then
		fgtadmin:Warn( callingPly, "You do not have the plugins privilege for this command!" )
		return
	end

	fgtadmin:CPrint( callingPly, "There are " .. #fgtadmin.plugins .. " plugins installed." )
	for _, plugin in pairs( fgtadmin.plugins ) do
		fgtadmin:CPrint( callingPly, plugin.name .. " : " .. plugin.desc )
	end
end
