FGTPLUGIN.name     = "Granted Commands"
FGTPLUGIN.desc     = "Print out a list of commands that the calling user has access to."
FGTPLUGIN.helptext = ""
FGTPLUGIN.commands = { "commands" }
FGTPLUGIN.privs    = { ["commands"] = fgtadmin.ranks.guest }

-- TODO: Fix or disable this plugin. Regardless if you have the privilege it will still print all commands associated with a plugin.
function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "commands" ) then
		fgtadmin:Warn( callingPly, "You do not have the commands privilege for this command!" )
		return
	end

	local grantedCommands = {}
	local playerPrivs = fgtadmin:GetPlayerPrivileges( callingPly )
	for _, priv in pairs( playerPrivs ) do
		for _, plugin in pairs( fgtadmin.plugins ) do
			for pluginPriv, rankTable in pairs( plugin.privs ) do
				if pluginPriv == priv then
					table.Add( grantedCommands, plugin.commands )
				end
			end
		end
	end

	local addThisCommand = true
	local uniqueCommandsStr = ""
	local uniqueCommands = {} -- For now each plugin can only have one primary command but multiple privileges.
				  -- If the user has multiple privileges relating to this plugin then only print the command once.
	for index, command in pairs( grantedCommands ) do
		for _, existingCommand in pairs( uniqueCommands ) do
			if command == existingCommand then -- This command is already in the table, move on.
				addThisCommand = false
				break
			end
		end
		if addThisCommand then	-- This command isn't already added to the uniqueCommands table, add it.
			table.insert( uniqueCommands, command )
			if index == 1 then -- Dont add the comma if this is the first command we're adding.
				uniqueCommandsStr = command
			else
				uniqueCommandsStr = uniqueCommandsStr .. ", " .. command
			end
		else
			addThisCommand = true -- Set the flag back to true for the next iteration.
		end
	end

	fgtadmin:Error( callingPly, "Commands: " .. uniqueCommandsStr )
end
