FGTPLUGIN.name     = "Ban"
FGTPLUGIN.desc     = "Ban a player from the server with a reason message."
FGTPLUGIN.helptext = "<player> <minutes> <reason>"
FGTPLUGIN.commands = { "ban" }
FGTPLUGIN.privs    = { -- The key is the privilege and the value is the default rank to give this to. (guest, admin, superadmin or owner)
			["ban"] = fgtadmin.ranks.superadmin, -- Permission to ban a user for less than a year.
			["permaban"] = fgtadmin.ranks.owner -- Permission to ban a user for MORE than a year.
}

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "ban" ) then -- You always should check if your callingPlayer has the privilege to use this command.
		fgtadmin:Warn( callingPly, "You do not have the ban privilege for this command!")
		return
	end
	if #args < 2 then -- Check if the callingPlayer has given enough arguments.
		fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
		return
	end

	-- args[1] contains the plugin command that executed this call. 'ban' in this example.
	local searchPlayer = args[2]
	local banDuration = args[3] and fgtadmin:ParseStringToMinutes( args[3] ) or 60
	local banReason = args[4] or "Banned by admin"

	local targetPlayer = fgtadmin:FindPlayersByName( searchPlayer, true )
	if not targetPlayer then
		fgtadmin:Error( callingPly, "No players found."  ) 
		return

	elseif targetPlayer == -1 then -- Multiple players found with similar name.
		fgtadmin:Error( callingPly, "Multiple players found with, " .. searchPlayer .. ", in their name. Be more specific." )
		return

	elseif targetPlayer:IsValid() and type(targetPlayer) == "Player" then
		if fgtadmin:PlayerCanTarget( callingPly, targetPlayer ) then
			local adminNick = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick()
			local actionVars = {}
			local formattedString = ""

			if banDuration == 0 then -- This is a perma ban
				if not fgtadmin:PlayerHasPrivilege( callingPly, "permaban" ) then
					fgtadmin:Warn( "You do not have the privilige to permanently ban!" )
					return
				end
				formattedString = "# has permanently banned # from the server. (#)" -- Admin has permanently banned Minge from the server. (Spamming.)
				actionVars = { {adminNick,Color(0,255,0)}, {targetPlayer:Nick(),Color(255,0,0)}, banReason }

			else
				formattedString = "# has banned # from the server for #. (#)" -- Admin has banned Minge from the server for 2 days. (Spamming.)
				local durationTable = fgtadmin:ParseMinutesToString( banDuration )
				local durationString = ""
				for index, unit in pairs( durationTable ) do
					if index == 1 then
						durationString = unit
					elseif index == #durationTable then
						durationString = durationString .. " and " .. unit
					else
						durationString = durationString .. ", " .. unit
					end
				end

				actionVars = { {adminNick,Color(0,255,0)}, {targetPlayer:Nick(),Color(255,0,0)}, durationString, banReason }
			end

			fgtadmin.bans[ targetPlayer:SteamID() ] = {	["name"] = targetPlayer:Nick(),
									["reason"] = banReason,
									["bandate"] = os.time(),
									["expirationdate"] = ((banDuration == 0) and 0 or (os.time() + (banDuration * 60)))
									-- If duration is 0 then the ban is permanent. Else convert minutes to seconds.
								  }
			fgtadmin:SaveBans()
			fgtadmin:KickIfBanned( targetPlayer )
			fgtadmin:Action( callingPly, fgtadmin:FormatAction(formattedString, actionVars), silentCall )

		else
			fgtadmin:Warn( callingPly, "You can't ban someone with greater or equal power level to you!" )
			return
		end
	end
end
