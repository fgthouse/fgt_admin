FGTPLUGIN.name     = "Modify Rank"
FGTPLUGIN.desc     = "Modify a ranks: privileges, title, or power level."
FGTPLUGIN.helptext = "<rank> <addpriv/rmpriv/setpower/settitle> <priv/power/title>"
FGTPLUGIN.commands = { "modrank" }
FGTPLUGIN.privs    = { ["modifyrank"] = fgtadmin.ranks.owner }

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "modifyrank" ) then
		fgtadmin:Warn( callingPly, "You do not have the modifyrank privilege for this command!")
		return

	end

	if #args < 4 then
		fgtadmin:Error(callingPly, "Usage: " .. self.helptext )
		return
	end

	local modRankStr = args[2]
	local modCommand = args[3]
	local modValue = args[4]

	if modRankStr == "owner" then -- Make sure ranks cant deny the owner rank of anything or change its power.
		if (modCommand == "rmpriv") or ((modCommand == "setpower") and (tonumber(modValue) < 100)) then
			fgtadmin:Warn( callingPly, "You can't deny the owner rank of anything!" )
			return
		end

		for _, priv in pairs( fgtadmin.ranks.owner.baseprivs ) do
			if modValue == priv then
				fgtadmin:Warn( callingPly, "You can't revoke the owner of their base permissions!" )
				return
			end
		end
	end

	local modRank = fgtadmin:GetRankTable( modRankStr )
	if modRank == nil then
		fgtadmin:Error( callingPly, "There is no " .. modRankStr .. " rank." )
		return
	end

	local modRanksPower = fgtadmin:GetRankPower( modRankStr ) -- The current power level of the target rank.
	local adminPower = fgtadmin:GetRankPower( fgtadmin:GetRank( callingPly ) ) -- The callingAdmin's power level.
	if adminPower <= modRanksPower then
		fgtadmin:Warn( callingPly, "You can't modify a rank of greater or equal power to your rank." )
		return
	end

	local adminName = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick() -- Get the admins name or "Console"
	local actionVars = { {adminName,Color(0,255,0)}, {modRankStr,Color(0,0,255)}, modValue } -- admin verbed ranks privilege

	if modCommand == "addpriv" then -- Add privilege to a rank
		local undeniedPriv = false
		for index, priv in pairs(modRank.deniedprivs) do -- Remove the denied priv if it exists.
			if priv == modValue then
				table.remove( modRank.deniedprivs, index )
				undeniedPriv = true
				break
			end
		end
		local inheritedPrivs = fgtadmin:GetRankPrivileges( modRank.name )
		for _, priv in pairs( inheritedPrivs ) do -- Check all privs, even the inherited ones.
			if priv == modValue then
				if undeniedPriv then -- We removed the priv from deniedprivs.
					fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# un-denied # the # privilege.", actionVars ) )
					fgtadmin:SaveRanks()
				else -- We're trying to give a rank privilege to one it already has...
					fgtadmin:Error( callingPly, modRank.name .. " already has the " .. priv .. " privilege." )
				end
				return
			end
		end
		table.insert( modRank.grantedprivs, modValue ) -- The rank isn't inheriting, being denied, or already have the priv. Grant it.
		fgtadmin:SaveRanks()
		fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# granted # the # privilege.", actionVars ) )
		return

	elseif modCommand == "rmpriv" then
		local inheritedPrivs = fgtadmin:GetRankPrivileges( modRank.name )
		for index, grantedPriv in pairs( modRank.grantedprivs ) do -- Remove a priv that has been granted with addpriv
			if grantedPriv == modValue then
				table.remove( modRank.grantedprivs, index )
				fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# has revoked #'s # privilege.", actionVars ), silentCall )
				fgtadmin:SaveRanks()
				return
			end
		end
		for _, grantedPriv in pairs(inheritedPrivs) do
			if grantedPriv == modValue then
				table.insert( modRank.deniedprivs, modValue ) -- Deny an inherited or base privilege.
				fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# has denied # the # privilege.", actionVars ), silentCall )
				fgtadmin:SaveRanks()
				return
			end
		end
		fgtadmin:Error( callingPly, modRankStr .. " doesn't have " .. modValue .. " privilege." )
		return

	elseif modCommand == "setpower" then
		local newPower = tonumber( modValue ) -- The requested power level
		local modRanksPower = fgtadmin:GetRankPower( modRankStr ) -- The current power level of the target rank.
		local adminPower = fgtadmin:GetRankPower( fgtadmin:GetRank( callingPly ) ) -- The callingAdmin's power level.
		local actionVars = { {adminName,Color(255,0,0)}, {modRankStr,Color(0,0,255)}, {tostring(newPower),Color(255,255,0)} }

		if modRanksPower == newPower then
			fgtadmin:Error( callingPly, modRankStr .. "'s power is already " .. tostring(newPower) .. "." )
			return
		end
		if newPower <= adminPower then
			modRank.power = newPower
			fgtadmin:SaveRanks()
			fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# set #'s power to #.", actionVars ), silentCall )
			return
		else
			fgtadmin:Warn( callingPly, "You can't set the power of a rank to a higher power than your rank!" )
			return
		end

	elseif modCommand == "settitle" then
		local actionVars = { {adminName,Color(255,0,0)}, {modRankStr,Color(0,0,255)}, {modValue,Color(255,255,0)} }
		modRank.title = modValue
		fgtadmin:SaveRanks()
		fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# set #'s title to #", actionVars ), silentCall )

	else
		fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
		return
	end
	fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
end
