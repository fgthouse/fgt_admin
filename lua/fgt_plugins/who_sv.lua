FGTPLUGIN.name     = "Who"
FGTPLUGIN.desc     = "Print out who is currently on the server and their rank."
FGTPLUGIN.helptext = ""
FGTPLUGIN.commands = { "who" }
FGTPLUGIN.privs    = { ["who"] = fgtadmin.ranks.guest }

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "who" ) then
		fgtadmin:Warn( callingPly, "You do not have the who privilege for this command!" )
		return
	end

	if callingPly:EntIndex() == 0 then
		print( "Playername:\t\t\tRank:" )
		for _, ply in pairs(player.GetAll()) do
			print( string.format( " %-20s\t\t %s", ply:Nick(), fgtadmin:GetRank( ply ) ) )
		end
	else
		fgtadmin:CPrint( callingPly, "Playername:\t\t\tRank:" )
		for _, ply in pairs(player.GetAll()) do
			fgtadmin:CPrint( callingPly, string.format( " %-20s\t\t %s", ply:Nick(), fgtadmin:GetRank( ply ) ) )
		end	
	end
end
