FGTPLUGIN.name     = "Disable"
FGTPLUGIN.desc     = "Disable a player; preventing them from spawning or interacting with objects or moving." -- TODO: Disable communication with everyone except the disabling admin.
FGTPLUGIN.helptext = "<player> <reason>"
FGTPLUGIN.commands = { "disable" }
FGTPLUGIN.privs    = {	["disable"] = fgtadmin.ranks.superadmin,
			["canUseToolgun"] = fgtadmin.ranks.guest, -- Toolgun in general
			["canUsePropertyMenu"] = fgtadmin.ranks.guest, -- C and right click menu
			["canDriveProps"] = fgtadmin.ranks.guest, -- C and right click then drive
			["canSpawnNpcs"] = fgtadmin.ranks.guest, -- NPC tab
			["canSpawnObjects"] = fgtadmin.ranks.guest, -- Spawnlists tab
			["canSpawnSents"] = fgtadmin.ranks.guest, -- Entities tab
			["canSpawnSweps"] = fgtadmin.ranks.guest, -- Weapons tab
			["canSpawnVehicles"] = fgtadmin.ranks.guest, -- Vehicles tab
			["canCommitSuicide"] = fgtadmin.ranks.guest, -- Kill/explode in console
			["canMoveAround"] = fgtadmin.ranks.guest, -- Any movement in general (wasd)
			["canNoClip"] = fgtadmin.ranks.guest, -- Noclip
			["canUseTool.*"] = fgtadmin.ranks.guest, -- Access to every toolgun. TODO: Move these to individual plugins.
			["canUseProperty.*"] = fgtadmin.ranks.guest, -- Access to property menu.
			["canSpawnNpc.*"] = fgtadmin.ranks.guest, -- All npcs
			["canSpawnSent.*"] = fgtadmin.ranks.guest,
			["canSpawnSwep.*"] = fgtadmin.ranks.guest,
			["canSpawnVehicle.*"] = fgtadmin.ranks.guest
}

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "disable" ) then -- You always should check if your callingPlayer has the privilege to use this command.
		fgtadmin:Warn( callingPly, "You do not have the disable privilege for this command!")
		return
	end

	if #args < 2 then -- Check if the callingPlayer has given enough arguments.
		fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
		return
	end

	local searchPlayer = args[2]
	local disableReason = args[3] or ""

	local player = fgtadmin:FindPlayersByName( searchPlayer, true ) -- Only want to disable one person
	if not player then
		fgtadmin:Error( callingPly, "No players found." )
		return
	elseif player == -1 then
		fgtadmin:Error( callingPly, "There are multiple matches for the player " .. searchPlayer .. ". Please be more specific." )
		return
	else
		if not fgtadmin:PlayerCanTarget( callingPly, player ) then 
			fgtadmin:Warn( callingPly, "You can't disable a player with a higher power level than you!" )
			return
		else
			-- TODO: revoke all privileges and strip weapons.
			-- Weapons can be given back by player_manager.RunClass( playerObject, "Loadout" )
			local formattedString = "# disabled #. (#.)"
			local adminNick = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick()
			local actionVars = { {adminNick,Color(0,255,0)}, {player:Nick(),Color(255,0,0)}, disableReason }
			fgtadmin:Action( callingPly, fgtadmin:FormatAction(formattedString, actionVars), silentCall )
		end
	end
end
