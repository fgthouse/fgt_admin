FGTPLUGIN.name     = "Ranks"
FGTPLUGIN.desc     = "Print out information on all the ranks privileges / title / and power."
FGTPLUGIN.helptext = ""
FGTPLUGIN.commands = { "ranks" }
FGTPLUGIN.privs    = { ["ranks"] = fgtadmin.ranks.admin }

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "ranks" ) then
		fgtadmin:Warn( callingPly, "You do not have the ranks privilege for this command!" )
		return
	end

	if callingPly:EntIndex() == 0 then
		print( "Power:  Rank:\t\tTitle:\t\tPrivileges:" )
		for _, rank in pairs(fgtadmin.ranks) do
			local grantedPrivs = fgtadmin:GetRankPrivileges( rank.name )
			local privsString = string.Implode( ", ", grantedPrivs )
			print( string.format( "%5.0d   %-15s %-15s %s", fgtadmin:GetRankPower( rank.name ), rank.name, rank.title, privsString ) )
		end
	else
		fgtadmin:CPrint( callingPly, "Power:  Rank:\t\tTitle:\t\tPrivileges:" )
		for _, rank in pairs(fgtadmin.ranks) do
			local grantedPrivs = fgtadmin:GetRankPrivileges( rank.name )
			local privsString = string.Implode( ", ", grantedPrivs )
			fgtadmin:CPrint( callingPly, string.format( "%5.0d   %-15s %-15s %s", fgtadmin:GetRankPower( rank.name ), rank.name, rank.title, privsString ) )
		end
		
	end
end
