FGTPLUGIN.name     = "Kick"
FGTPLUGIN.desc     = "Kick a player from the server with a reason message."
FGTPLUGIN.helptext = "<player> <reason>"
FGTPLUGIN.commands = { "kick" }
FGTPLUGIN.privs    = { ["kick"] = fgtadmin.ranks.admin } -- This will give the default ranks the privileges to use certain plugin 'functions'. See ban_sv.lua for a better example.

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "kick" ) then -- You always should check if your callingPlayer has the privilege to use this command.
		fgtadmin:Warn( callingPly, "You do not have the kick privilege for this command!")
		return
	end

	if #args < 2 then -- Check if the callingPlayer has given enough arguments.
		fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
		return
	end

	-- args[1] contains the command that caused this execution. 'kick' in this example.
	local searchPlayer = args[2]
	local kickReason = args[3] or "Kicked by admin."

	local player = fgtadmin:FindPlayersByName( searchPlayer, true ) -- Only want to kick one person
	if not player then
		fgtadmin:Error( callingPly, "No players found." )
		return
	elseif player == -1 then
		fgtadmin:Error( callingPly, "There are multiple matches for the player " .. searchPlayer .. ". Please be more specific." )
		return
	else
		if not fgtadmin:PlayerCanTarget( callingPly, player ) then -- Make sure we're not kicking someone of a higher level than us.
			fgtadmin:Warn( callingPly, "You can't kick a player with a higher power level than you!" )
			return
		else
			player:Kick( kickReason )
			local formattedString = "# kicked # from the server. (#)"
			local adminNick = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick()
			local actionVars = { {adminNick,Color(0,255,0)}, {player:Nick(),Color(255,0,0)}, kickReason } -- kickReason will print in white, if you don't specify a color it defaults to this.
			fgtadmin:Action( callingPly, fgtadmin:FormatAction(formattedString, actionVars), silentCall )
		end
	end
end
