FGTPLUGIN.name     = "Rank"
FGTPLUGIN.desc     = "Allows you to set other players rank up to your own rank."
FGTPLUGIN.helptext = "<player> <rank>"
FGTPLUGIN.commands = { "rank" }
FGTPLUGIN.privs    = { ["rank"] = fgtadmin.ranks.superadmin }

function FGTPLUGIN:Main( callingPly, args, silentCall, calledByChat )
	if not fgtadmin:PlayerHasPrivilege( callingPly, "rank" ) then
		fgtadmin:Warn( callingPly, "You do not have the rank privilege for this command!" )
		return
	end

	if #args < 3 then
		fgtadmin:Error( callingPly, "Usage: " .. self.helptext )
		return
	end

	local searchPlayer = args[2]
	local targetRank = args[3]
	local targetPlayer = fgtadmin:FindPlayersByName( searchPlayer, true )

	if not targetPlayer then
		fgtadmin:Error( callingPly, "Player not found!" )
		return
	end

	if callingPly == targetPlayer then
		fgtadmin:Warn( callingPly, "You can't set your own rank." )
		return
	end

	if not fgtadmin:RankExists( targetRank ) then
		fgtadmin:Error( callingPly, "Rank: " .. targetRank .. " doesn't exist!" )
		return
	end


	local callingPlyPower = fgtadmin:GetRankPower( fgtadmin:GetRank( callingPly ) )
	local targetRankPower = fgtadmin:GetRankPower( targetRank )

	if not fgtadmin:PlayerCanTarget( callingPly, targetPlayer ) then
		fgtadmin:Warn( callingPly, "You can't change the rank of someone with greater than or equal power to you!" )
		return
	elseif targetRankPower >= callingPlyPower then
		fgtadmin:Warn( callingPly, "You can't set a players rank greater than or equal to yours!" )
		return
	end

	local adminNick = (callingPly:EntIndex() == 0) and "Console" or callingPly:Nick()
	local actionVars = { {adminNick,Color(0,255,0)}, { targetPlayer:Nick(), Color(0,0,255)}, targetRank }

	local targetPlayerInfoTable = fgtadmin.users[ targetPlayer:SteamID() ]

	if targetRank == "guest" then -- Demoting someone to guest
		if targetPlayerInfoTable then -- targetPlayerTable is not nil if the user already exists in our users table.
			targetPlayer:SetFgtRank( targetRank )
			fgtadmin.users[ targetPlayer:SteamID() ] = nil -- Remove this user from our users table
			fgtadmin:SaveUsers()
			fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# demoted # back to #.", actionVars ), silentCall )
			return
		end

		fgtadmin:Error( callingPly, "User " .. targetPlayer:Nick() .. " is already guest." )
		return
	end

	if targetPlayerInfoTable then -- Check if the user is already in our users table.
		if fgtadmin:GetRankPower( targetPlayerInfoTable.rank ) < fgtadmin:GetRankPower( targetRank ) then
			fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# promoted # to #.", actionVars ), silentCall )

		elseif fgtadmin:GetRankPower( targetPlayerInfoTable.rank ) == fgtadmin:GetRankPower( targetRank ) then
			fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# changed #'s rank to #.", actionVars ), silentCall )

		else
			fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# demoted # to #.", actionVars ), silentCall )
		end

		targetPlayerInfoTable.rank = targetRank -- Update the users rank
		targetPlayer:SetFgtRank( targetRank )
		fgtadmin:SaveUsers()

		return
	end

	-- We never found this user in our users table. Add him.
	local newUser = { ["name"] = targetPlayer:Nick(), ["rank"] = targetRank, ["grantedprivs"] = {}, ["deniedprivs"] = {} }
	fgtadmin.users[ targetPlayer:SteamID() ] = newUser
	targetPlayer:SetFgtRank( targetRank )
	fgtadmin:SaveUsers()
	fgtadmin:Action( callingPly, fgtadmin:FormatAction( "# added # to the # rank.", actionVars ), silentCall )
end
