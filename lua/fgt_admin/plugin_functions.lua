-- Check if a rank exists.
function fgtadmin:RankExists( checkRank ) -- string
	for _, rank in pairs(self.ranks) do
		if rank.name == checkRank then
			return true
		end
	end

	return false
end

-- We should call this instead of player:GetFgtRank() for console support.
function fgtadmin:GetRank( player ) -- player object (or Entity(0) if console)
	if (( type(player) == "Entity" ) and ( player:EntIndex() == 0 )) or player:IsListenServerHost() then -- This is the console or listen host.
		return "owner"
	else
		return player:GetFgtRank() or "guest"
	end
end

-- Return a ranks power level (integer)
function fgtadmin:GetRankPower( playerRank ) -- string
	if self:RankExists( playerRank ) then
		for _, rank in pairs(self.ranks) do
			if rank.name == playerRank then
				return rank.power
			end
		end
	else
		return nil
	end

	return 0
end

-- Grant targetPlayer privilege in the users table.
function fgtadmin:PlayerAddPrivilege( targetPlayer, privilege ) -- player, string
	if self:PlayerHasPrivilege( targetPlayer, privilege ) then
		print( "The player already has this privilege." )
		return
	end

	if type(targetPlayer) ~= "Player" then -- Can't add privileges to a entity (Console).
		return
	end

	local targetPlayerInfoTable = self.users[ targetPlayer:SteamID() ]
	if targetPlayerInfoTable then -- This player is already in our users table..
		if table.HasValue( targetPlayerInfoTable.deniedprivs, privilege ) then -- This player is currently denied the priv, remove it then check if it's being inherited.
			print( "Removing denied priv." )
			table.RemoveByValue( targetPlayerInfoTable.deniedprivs, privilege )
		end
		if not self:PlayerHasPrivilege( targetPlayer, privilege ) then -- The player isn't inheriting the privilege so lets add it to the grantedprivs.
			print( "Granting priv." )
			table.insert( targetPlayerInfoTable.grantedprivs, privilege )
		end
		if ((#targetPlayerInfoTable.grantedprivs + #targetPlayerInfoTable.deniedprivs) == 0) and (targetPlayerInfoTable.rank == "guest") then -- This player is back to default guest privileges.
			self.users[ targetPlayer:SteamID() ] = nil; -- Remove the player from our users table.
		end
	else -- Add this user
		local newUser = { ["name"] = targetPlayer:Nick(), ["rank"] = self:GetRank( targetPlayer ), ["grantedprivs"] = { privilege }, ["deniedprivs"] = {} }
		fgtadmin.users[ targetPlayer:SteamID() ] = newUser
	end
	self:SaveUsers()
end

-- Revoke or deny targetPlayer of privilege in the fgtadmin.users table.
function fgtadmin:PlayerRemovePrivilege( targetPlayer, privilege ) -- player, string
	if not self:PlayerHasPrivilege( targetPlayer, privilege ) then
		print( "The player doesn't have this privilege." )
		return
	end

	if type(targetPlayer) ~= "Player" then -- Can't add privileges to a entity (Console).
		return
	end

	local targetPlayerInfoTable = self.users[ targetPlayer:SteamID() ]
	if targetPlayerInfoTable then -- This player is already in our users table.
		if table.HasValue( targetPlayerInfoTable.grantedprivs, privilege ) then -- This player has the privilege in their granted table. Remove it.
			print( "Removing the granted priv." )
			table.RemoveByValue( targetPlayerInfoTable.grantedprivs, privilege )
		end
		if self:PlayerHasPrivilege( targetPlayer, privilege ) then -- The player is still inheriting the privilege. Deny it.
			print( "Denying the priv." )
			table.insert( targetPlayerInfoTable.deniedprivs, privilege )
		end
		if ((#targetPlayerInfoTable.grantedprivs + #targetPlayerInfoTable.deniedprivs) == 0) and (targetPlayerInfoTable.rank == "guest") then -- This player is back to default guest privileges.
			self.users[ targetPlayer:SteamID() ] = nil; -- Remove the player from our users table.
		end
	else -- Add this user
		local newUser = { ["name"] = targetPlayer:Nick(), ["rank"] = self:GetRank( targetPlayer ), ["grantedprivs"] = {}, ["deniedprivs"] = { privilege } }
		fgtadmin.users[ targetPlayer:SteamID() ] = newUser
	end
	self:SaveUsers()
end

-- Check if the callingPlayer has greater power than the targetPlayer.
function fgtadmin:PlayerCanTarget( callingPlayer, targetPlayer ) -- player, player
	local callingPlyPower = self:GetRankPower( self:GetRank( callingPlayer ) )
	local targetPlyPower = self:GetRankPower( self:GetRank( targetPlayer ) )

	if targetPlyPower >= callingPlyPower then -- Don't let ranks target equal power ranks.
		return false
	end
	return true
end

-- Check if the user has this privilege.
-- TODO: Rework privilege system for effeciency.
-- rank.baseprivs = { ["canUseTool"] = {"wire_*", "igniter", "balloon"}, ["canNoClip"] = true, ["canMoveAround"] = false }
function fgtadmin:PlayerHasPrivilege( targetPlayer, targetPriv ) -- player, string
	if (targetPlayer:EntIndex() == 0) or targetPlayer:IsListenServerHost() then -- Console has all privileges.
		return true
	end

	local targetPlayersPrivileges = self:GetPlayerPrivileges( targetPlayer ) -- Get granted privileges for a player
	local targetPlayersInfoTable = self.users[ targetPlayer:SteamID() ] -- Get players grant/deny table to parse any wildcard privileges.
	local targetPlayersRankTable = self:GetRankTable( self:GetRank( targetPlayer ) ) -- Get any rank wildcard privileges.

	if targetPlayersInfoTable then -- Check if this player has any user denied privileges.
		for _, denyPriv in pairs( targetPlayersInfoTable.deniedprivs ) do -- Remove any denied privileges.
			if targetPriv == denyPriv then -- Deny if the privilege is in the players deniedprivs table.
				return false
			end

			if string.match( denyPriv, "%*" ) and self:WildcardPrivilegeMatches( targetPriv, denyPriv ) then -- Check if this playerss wildcard deny privilege matches our targetPriv
				--print( "Denying privilege from regex " .. targetPriv .. " : " .. denyPriv )
				return false
			end
			--table.RemoveByValue( targetPlayersPrivileges, denyPriv ) -- Remove any privileges that are in targetPlayersPrivileges that are also in the players deniedprivs table.
		end
	end

	if #targetPlayersRankTable.deniedprivs > 0 then -- Check for any rank denied privileges.
		for _, denyPriv in pairs( targetPlayersRankTable.deniedprivs ) do
			if targetPriv == denyPriv then
				return false
			end

			if string.match( denyPriv, "%*" ) and self:WildcardPrivilegeMatches( targetPriv, denyPriv ) then
				--print( "Denying privilege from regex " .. targetPriv .. " : " .. denyPriv )
				return false
			end
		end
	end

	for _, privName in pairs( targetPlayersPrivileges ) do -- Iterate through all the privs this player has looking for a match.
		if privName == targetPriv then -- This user does have the targetPriv privilege.
			return true
		end
		if string.match( privName, "%*" ) and fgtadmin:WildcardPrivilegeMatches( targetPriv, privName ) then
			--print( "Granting privilege from regex " .. targetPriv .. " : " .. privName )
			return true
		end
	end

	return false -- The user isn't being denied the privilege but also isn't inheriting it.
end
