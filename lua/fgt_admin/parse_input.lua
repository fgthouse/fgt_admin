-- This file will contain the functions for parsing player or time arguments.
-- Assisting in multi-target plugins or converting human readable times to minutes.
-- This should be called by plugins to support target modifiers. (#guests+Sean)
function fgtadmin:ParseName( name, oneTarget ) -- string
	-- Support wildcards
	-- Support steamids
	-- Support ranks
	-- Support all the things
end

-- This will take an integer, in minutes. And return a table of human readable strings. This is the inverse of ParseTimeToMinutes()
function fgtadmin:ParseMinutesToString( timeMinutes, maxUnits ) -- integer, integer
	local timeTable = {}
	if not maxUnits then -- Return all the units we can.
		maxUnits = 7
	end

	local years = math.floor( timeMinutes / 483840 ) -- 483840 minutes in a year
	timeMinutes = timeMinutes - (years * 483840) -- subtract x years.
	if (years > 0) and (maxUnits > 0) then -- is x minutes enough to be at least 1 year? And have we hit our maxUnits limit yet?
		maxUnits = maxUnits - 1 -- We're adding this unit. Go ahead and subtract.
		if (maxUnits <= 0) and (timeMinutes > 0) then -- Is this unit the last one we're adding? And is there still more time left?
			years = years + 1 -- If so round up the value
		end
		table.insert( timeTable, tostring( years ) .. ((years == 1) and " year" or " years") ) -- insert our string. (x year[s])
	end
	
	local months = math.floor( timeMinutes / 40320 ) -- 40320 minutes in a month
	timeMinutes = timeMinutes - ( months * 40320 )
	if (months > 0) and (maxUnits > 0) then
		maxUnits = maxUnits - 1
		if (maxUnits <= 0) and (timeMinutes > 0) then
			months = months + 1
		end
		table.insert( timeTable, tostring( months ) .. ((months == 1) and " month" or " months" ) )
	end
	
	local weeks = math.floor( timeMinutes / 10080 ) -- 10080 minutes in a week
	timeMinutes = timeMinutes - ( weeks * 10080 )
	if (weeks > 0) and (maxUnits > 0) then
		maxUnits = maxUnits - 1
		if (maxUnits <= 0) and (timeMinutes > 0) then
			weeks = weeks + 1
		end
		table.insert( timeTable, tostring( weeks ) .. ((weeks == 1) and " week" or " weeks" ) )
	end
	
	local days = math.floor( timeMinutes / 1440 ) -- 1440 minutes in a day
	timeMinutes = timeMinutes - ( days * 1440 )
	if (days > 0) and (maxUnits > 0) then
		maxUnits = maxUnits - 1
		if (maxUnits <= 0) and (timeMinutes > 0) then
			days = days + 1
		end
		table.insert( timeTable, tostring( days ) .. ((days == 1) and " day" or " days" ) )
	end
	
	local hours = math.floor( timeMinutes / 60 ) -- 60 minutes in an hour
	timeMinutes = timeMinutes - ( hours * 60 )
	if (hours > 0) and (maxUnits > 0) then
		maxUnits = maxUnits - 1
		if (maxUnits <= 0) and (timeMinutes > 0) then
			hours = hours + 1
		end
		table.insert( timeTable, tostring( hours ) .. ((hours == 1) and " hour" or " hours" ) )
	end
	
	local minutes = math.floor( timeMinutes ) -- Decimal place is seconds so floor the value.
	timeMinutes = timeMinutes - minutes
	if (minutes > 0) and (maxUnits > 0) then
		maxUnits = maxUnits - 1
		if (maxUnits <= 0) and (timeMinutes > 0) then
			minutes = minutes + 1
		end
		table.insert( timeTable, tostring( minutes ) .. ((minutes == 1) and " minute" or " minutes" ) )
	end
	
	local seconds = math.floor( timeMinutes * 60 )
	if (seconds > 0) and (maxUnits > 0) then
		table.insert( timeTable, tostring( seconds ) .. ((seconds == 1) and " second" or " seconds" ) )
	end
	
	return timeTable
end

-- This will take various time format strings and return an integer in minutes. (1 day, 2 hours, 1 year, 10 minutes, 1 minute, perm, forever ect.)
function fgtadmin:ParseStringToMinutes( timeString ) -- string
	local minutes = tonumber( timeString )
	if minutes and ((minutes > 0) and (minutes < math.huge)) then
		return minutes
	elseif minutes and (minutes >= math.huge) then
		return 0
	else
		minutes = 1
	end
	
	if string.find( timeString, "a[n]?" ) or string.find( timeString, "1 " ) then -- a month, an hour, 1 year. Catch all the singles.
		minutes = 1
	elseif string.find( timeString, "%d+" ) then -- 12 years, 2 minutes. %d matches numeric digits
		minutes = tonumber( string.match( timeString, "%d+" ) ) -- Get the number of days/minutes/whatver to do.
	else
		minutes = 1
	end
	
	if string.find( timeString, "second[s]?" ) then
		return minutes / 60
	elseif string.find( timeString, "minute[s]?" ) then
		return minutes
	elseif string.find( timeString, "hour[s]?" ) then
		return minutes * 60 -- 60 minutes in an hour
	elseif string.find( timeString, "day[s]?" ) then
		return minutes * 1440 -- 1440 minutes in a day (12 hours in a day)
	elseif string.find( timeString, "week[s]?" ) then
		return minutes * 10080 -- 10080 minutes in a week (7 days in a week)
	elseif string.find( timeString, "month[s]?" ) then
		return minutes * 40320 -- 40320 minutes in a month (4 weeks in a month)
	elseif string.find( timeString, "year[s]?" ) then
		return minutes * 483840 -- 483840 minutes in a year (12 months in a year)
	else
		return 0
	end
end

-- Takes a privilege with a wildcard in it and a regular privilege then checks if they match in pattern.
function fgtadmin:WildcardPrivilegeMatches( targetPriv, wildcardPriv ) -- string, string
	local escapedString = string.Replace( wildcardPriv, ".", "%." ) -- Escape .
	escapedString = string.Replace( escapedString, "*", ".+" ) -- Replace * with .+ aka any character one or more times. (Not 0 or more to prevent `canUseTool.` from being true.)

	if string.match( targetPriv, escapedString ) then
		return true
	end
	return false
end
