
-- Load the ranks/groups file.
function fgtadmin:LoadRanks() 
	local ranksVon = file.Read( "fgtadmin_ranks.txt", "DATA" )
	if (ranksVon == nil) or (ranksVon == "") then
		self:Print("Failed to load the ranks file. DEFAULTING!")
		include("fgtadmin_defaultranks.lua") -- This will set fgtadmin.ranks to the default ranks.
		file.Write( "fgtadmin_ranks.txt", von.serialize( self.ranks ) )
	else
		self:Print( "Loaded ranks file." )
		self.ranks = von.deserialize( ranksVon )
	end
end

-- Save the ranks table to a file.
function fgtadmin:SaveRanks()
	local tmpRanks = table.Copy( self.ranks ) -- Create a copy so we can remove the baseprivs and save. (baseprivs are initialized on startup)
	for _, rank in pairs( tmpRanks ) do
		table.Empty( rank.baseprivs )
	end
	file.Write( "fgtadmin_ranks.txt", von.serialize( tmpRanks ) )
end

-- Load the users file. (Contains a json table of anyone above guest rank)
function fgtadmin:LoadUsers() 
	local usersVon = file.Read( "fgtadmin_users.txt", "DATA" )
	if (usersVon == nil) or (usersVon == "") then
		file.Write( "fgtadmin_users.txt", von.serialize( self.users ) )
	else
				table.remove( self.plugins, index )
		self:Print( "Loaded users file." )
		self.users = von.deserialize( usersVon )
	end
end

-- Save the users table to a file.
function fgtadmin:SaveUsers()
	file.Write( "fgtadmin_users.txt", von.serialize( self.users ) )
end

-- Load the banned users into a table.
function fgtadmin:LoadBans()
	local bansVon = file.Read( "fgtadmin_bans.txt", "DATA" )
	if (bansVon == nil) or (bansVon == "") then
		file.Write( "fgtadmin_bans.txt", von.serialize( self.bans ) )
	else
		self:Print( "Loaded bans file." )
		self.bans = von.deserialize( bansVon )
	end
end

-- Save the banned users table to a file file.
function fgtadmin:SaveBans()
	file.Write( "fgtadmin_bans.txt", von.serialize( self.bans ) )
end

-- Load the plugins and send them to the client if needed.
function fgtadmin:LoadPlugins()
	self:Print( "Loading Plugins:" )
	AddCSLuaFile( "autorun/client/fgtadmin_cl.lua" ) -- Include the file that loads all the clientside plugins.

	local plugins, dirs = file.Find( "fgt_plugins/*", "LUA" ) 
	for _, plugin in pairs( plugins ) do
		if string.EndsWith( plugin, "_sv.lua" ) or string.EndsWith( plugin, "_sh.lua" ) then
			FGTPLUGIN = self:NewPlugin() -- Define a global table for our plugin to re-define internally.
			FGTPLUGIN.fileName = plugin -- Set the file name of this plugin. Useful for debugging which plugin is breaking something.
			local pluginFunc = CompileFile( "fgt_plugins/" .. plugin ) -- Returns a function or nil if file encounters an error. Calling the function is like include()
			if pluginFunc then -- Only load the plugin if it didn't error when doing CompileFile()
				pluginFunc() -- Execute our plugin and let it initialize.
				table.insert( self.plugins, FGTPLUGIN ) -- Insert this plugin into our plugins table.
				self:Print(" " .. plugin)
			else
				self:Print( " The plugin " .. plugin .. " has encountered an error. Disabling plugin!" )
			end
			FGTPLUGIN = nil -- Undefine our global table.

		elseif string.EndsWith( plugin, "_cl.lua" ) or string.EndsWith( plugin, "_sh.lua" ) then
			AddCSLuaFile( "fgt_plugins/" .. plugin )
			self:Print(" Sending plugin: " .. plugin .. " to the client.")

		else
			self:Print( " Failed to read suffix on plugin! [" .. plugin .. "]" )
		end
	end

	self:Print( "Checking Plugins for Errors:" )
	local existingCommands = {}
	for index, plugin in pairs(self.plugins) do
		local enablePlugin = true

		for key, command in pairs( plugin.commands ) do -- Check if every plugin has unique commands
			if command == "default_command" then
				self:Print( " The plugin " .. plugin.fileName .. " is trying to use the default_command. Disabling plugin!" )
				enablePlugin = false
				break
			elseif table.HasValue( existingCommands, command ) then
				self:Print( " The plugin " .. plugin.fileName .. " is trying to use an already in use command, " .. command .. ". Disabling plugin!" )
				enablePlugin = false
				break
			end
		end

		for priv, rank in pairs( plugin.privs ) do -- Check if every plugin has a properly defined privs table.
			if priv == "default_privilege" then
				self:Print( " The plugin " .. plugin.fileName .. " is trying to use the default_privilege. Disabling plugin!" )
				enablePlugin = false
				break					-- Move on the the next plugin.
			elseif (type(priv) ~= "string") and (type(rank) ~= "table") then -- Make sure the privs table is properly declared.
				self:Print( " The plugin " .. plugin.fileName .. " has a malformed privs table. Disabling plugin!" )
				enablePlugin = false
				break
			end

		end

		if not enablePlugin then -- Remove this plugin due to errors.
			table.remove( self.plugins, index )
		else
			table.Add( existingCommands, plugin.commands ) -- The plugin doesn't have duplicate commands. Add these commands to our table to be checked against other plugins.
			for priv, rank in pairs( plugin.privs ) do
				table.insert( rank.baseprivs, priv ) -- Add this plugins privilege[s] to the requested base ranks.
			end
		end
	end

	self:Print( "Done Loading Plugins!" )
end

-- Create a new default plugin, add it to our plugins table, then return it for the plugin writer to modify.
function fgtadmin:NewPlugin()
	local plug = {}	-- The plugin's table.
	plug.name = "DEFAULT_PLUGIN" -- The name for the plugin. Used for the gui or the plugins command. This is presented to the user.
	plug.desc = "THIS IS A PLUGIN THAT HAS NOT BEEN CHANGED. IT DOES NOTHING.!" -- A description that can be presented to the user.
	plug.commands = { "default_command" } -- This is the chat or console command used to call the plugins Main()
	plug.helptext = "YELL AT THE PLUGIN AUTHOR TO FIX THIS." -- This is the text you should return to the user if they don't give the right arguments. A usage string.
	plug.privs = { ["default_privilege"] = self.ranks.owner } -- This is the privilege required to use this plugin. You should have a custom privilege for each plugin.
	plug.hasErrored = true -- This is used internally by the LoadPlugins() function to check if a plugin initialized with no errors. You should set this at the end of the plugin.
	plug.fileName = "nonexistant_sv.lua" -- This will hold the plugins file name in the fgt_plugins directory.
	plug.Main = function() self:Print("!~!~ THERE IS A PLUGIN INSTALLED THAT DOES NOTHING ~!~!") end -- The entry function to run on plugin call.

	return plug
end
