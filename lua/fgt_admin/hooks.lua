local DEBUG_HOOKS = false

hook.Add( "PlayerInitialSpawn", "FgtAdmin_PlayerInitialSpawn", function( spawningPlayer )
	fgtadmin:KickIfBanned( spawningPlayer )
end )

hook.Add( "PlayerAuthed", "FgtAdmin_PlayerAuthed", function( authPlayer )
	if (not authPlayer:IsValid()) then -- In the event that PlayerAuthed fails to give a valid player object.
		ErrorNoHalt( "PlayerAuthed HOOK CALLED WITH AN INVALID PLAYER!!!" )
		timer.Simple( 1, function( authPlayer ) -- Check again every second until the player object becomes valid.
			hook.Call( "PlayerAuthed", GAMEMODE, authPlayer )
		end )
		return
	end

	-- TODO: Have a table of players that are connected through family sharing. ["owners_steamid"] = { "child_steamid", "child2_steamid" }
	fgtadmin:KickIfBanned( authPlayer )

	local spawningPlayerInfoTable = fgtadmin.users[ authPlayer:SteamID() ]
	if spawningPlayerInfoTable and ( spawningPlayerInfoTable.rank ~= "guest" ) then -- Don't notify guests if they're in our user table. (Because they've been denied/granted something.)
		authPlayer:SetFgtRank( spawningPlayerInfoTable.rank )
		fgtadmin:CPrint( authPlayer, "Set your rank to " .. spawningPlayerInfoTable.rank .. "." )
	end
end )

hook.Add( "SetupMove", "FgtAdmin_SetupMove", function( movingPlayer, clientMoveData )
	if not fgtadmin:PlayerHasPrivilege( movingPlayer, "canMoveAround" ) then
		clientMoveData:SetForwardSpeed( 0 )
		clientMoveData:SetSideSpeed( 0 )
		clientMoveData:SetUpSpeed( 0 )
		return true
	end
end )

hook.Add( "PlayerNoClip", "FgtAdmin_PlayerNoClip", function( noclippingPlayer, desiredState )
	if not fgtadmin:PlayerHasPrivilege( noclippingPlayer, "canNoClip" ) then
		return false
	end
end )

-- Useless hooks? PlayerSpawnEffect, PlayerSpawnProp, PlayerSpawnRagdoll
-- TODO: CanTool hook will also need to support individual tools, not just the toolgun as a whole.
-- Check if the toolingPlayer has the privilege to use the toolgun.
hook.Add( "CanTool", "FgtAdmin_CanTool", function( toolingPlayer, playersTrace, toolMode ) -- canUseToolgun
	if DEBUG_HOOKS then print( "[HOOK] CanTool: " .. toolMode ) end
	if not fgtadmin:PlayerHasPrivilege( toolingPlayer, "canUseToolgun" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( toolingPlayer, "canUseTool." .. toolMode ) then
		return false
	end
end )

-- Check if the toolingPlayer has the privilege to use the property menu (C and Right Click).
hook.Add( "CanProperty", "FgtAdmin_CanProperty", function( toolingPlayer, propertyMode, targetEntity ) -- canUsePropertyMenu
	if DEBUG_HOOKS then print( "[HOOK] CanProperty: " .. propertyMode ) end
	if not fgtadmin:PlayerHasPrivilege( toolingPlayer, "canUsePropertyMenu" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( toolingPlayer, "canUseProperty." .. propertyMode ) then
		return false
	end
end )

-- Check if the drivingPlayer has the privilege to drive props.
hook.Add( "CanDrive", "FgtAdmin_CanDrive", function( drivingPlayer, targetEntity ) -- canDriveProps
	if DEBUG_HOOKS then print( "[HOOK] CanDrive: " .. targetEntity:EntIndex() ) end
	if not fgtadmin:PlayerHasPrivilege( drivingPlayer, "canDriveProps" ) then
		return false
	end
end )

-- Check if the creatingPlayer has the privilege to spawn npcs.
hook.Add( "PlayerSpawnNPC", "FgtAdmin_PlayerSpawnNPC", function( creatingPlayer, npcType, weaponName ) -- canSpawnNpcs
	if DEBUG_HOOKS then print( "[HOOK] PlayerSpawnNPC: " .. npcType ) end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnNpcs" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnNpc." .. npcType ) then
		return false
	end
end )

-- Check if the creatingPlayer has the privilege of spawning objects. 
hook.Add( "PlayerSpawnObject", "FgtAdmin_PlayerSpawnObject", function( creatingPlayer, modelName, skinNumber ) -- canSpawnObjects
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnObjects" ) then
		return false
	end
	-- TODO: canSpawnObject.* ?
end )

-- Check if the creatingPlayer has the privilege of spawning scripted entities. 
hook.Add( "PlayerSpawnSENT", "FgtAdmin_PlayerSpawnSENT", function( creatingPlayer, sentClass ) -- canSpawnSents
	if DEBUG_HOOKS then print( "[HOOK] PlayerSpawnSENT: " .. sentClass ) end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnSents" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnSent." .. sentClass ) then
		return false
	end
end )

-- Check if the creatingPlayer has the privilege of spawning scripted weapons.
hook.Add( "PlayerSpawnSWEP", "FgtAdmin_PlayerSpawnSWEP", function( creatingPlayer, weaponName, infoNumber ) -- canSpawnSweps
	if DEBUG_HOOKS then print( "[HOOK] PlayerSpawnSWEP: " .. weaponName ) end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnSweps" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnSwep." .. weaponName ) then
		return false
	end
end )

-- Check if the creatingPlayer has the privilege of giving themselves scripted weapons.
hook.Add( "PlayerGiveSWEP", "FgtAdmin_PlayerGiveSWEP", function( creatingPlayer, weaponName, weaponInfo ) -- canSpawnSweps
	if DEBUG_HOOKS then print( "[HOOK] PlayerGiveSWEP: " .. weaponName ) end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnSweps" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnSwep." .. weaponName ) then
		return false
	end
end )

-- Check if the creatingPlayer has the privilege of spawning vehicles.
hook.Add( "PlayerSpawnVehicle", "FgtAdmin_PlayerSpawnVehicle", function( creatingPlayer, modelName, vehicleName, vehicleInfo ) -- canSpawnVehicles
	if DEBUG_HOOKS then print( "[HOOK] PlayerSpawnVehicle: " .. vehicleName ) end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnVehicles" ) then
		return false
	end
	if not fgtadmin:PlayerHasPrivilege( creatingPlayer, "canSpawnVehicle." .. vehicleName ) then
		return false
	end
end )

-- Check if dyingPlayer has the privilige of kill/explode.
hook.Add( "CanPlayerSuicide", "FgtAdmin_CanPlayerSuicide", function( dyingPlayer ) -- canCommitSuicide
	if not fgtadmin:PlayerHasPrivilege( dyingPlayer, "canCommitSuicide" ) then
		return false
	end
end )

-- Check if the talkingPlayer has the privilege of using voice chat.
-- TODO: Will this become a bottleneck on high slot servers?
hook.Add( "PlayerCanHearPlayersVoice", "FgtAdmin_PlayerCanHearPlayer", function( listeningPlayer, talkingPlayer ) -- canVoiceChat
	if listeningPlayer == talkingPlayer then -- Why does it even check if you can hear yourself?
		return true
	end
	if not fgtadmin:PlayerHasPrivilege( talkingPlayer, "canVoiceChat" ) then
		return false
	end
end )

-- Check if the chattingPlayer has the privilege of using text chat.
hook.Add( "PlayerSay", "FgtAdmin_OnPlayerChatMuted", function( chattingPlayer, text, teamChat ) -- canTextChat
	if not fgtadmin:PlayerHasPrivilege( chattingPlayer, "canTextChat" ) then
		return ""
	end
end )

-- This will detect chat commands. The ugly for loop will find double quotes and turn anything inside of them into one argument and pass it on.
hook.Add( "PlayerSay", "FgtAdmin_OnPlayerChat", function( chattingPlayer, text, teamChat )
	local firstChar = string.Left( text, 1 )
	if (firstChar == "@") or (firstChar == "!") then -- The player wants to execute a command.
		local isSilent = (firstChar == "@") and true or false -- Silent if the command begins with @
		local arguments = {  } -- Our table of arguments to pass to fgtAdminCommand which then get passed to the plugin.
		local quotedString = "" -- This will hold our quoted argument we're building.
		local needClosingQuote = false -- We're waiting on the ending quote.
		local explodeText = string.Explode( " ", string.Right( text, #text-1 ) ) -- This separates by spaces and removes the leading ! or @

		for _, text in pairs(explodeText) do
			if not needClosingQuote and (string.Left( text, 1 ) == "\"") then -- Have we hit a " and now need to look for the closing " ?
				quotedStr = string.sub( text, 2 ) -- Dont save the leading "
				needClosingQuote = true -- This flag says that we're looking for a trailing "
			elseif needClosingQuote and (string.Right( text, 1 ) == "\"") then -- Are we looking for closing quotes and have we found a trailing " ?
				quotedStr = quotedStr .. " " .. string.sub( text, 1, #text-1 ) -- Put a space between the previous text and remove the trailing "
				table.insert( arguments, quotedStr ) -- Put it into our argument table.
				quotedStr = ""
				needClosingQuotes = false
			elseif needClosingQuote then
				quotedStr = quotedStr .. " " .. text -- Add the text in between two quotes.
			else
				table.insert( arguments, text ) -- This is a single word argument.
			end
		end
		fgtAdminCommand( chattingPlayer, isSilent and "fas" or "fa", arguments ) -- Pass the chat command to the fgtAdminCommand function, which also gets called on the fa[s] concommands.
		if isSilent then -- Don't print anything beginning with @ to chat. Avoids mistyping admin commands and telling everyone.
			return ""
		end
	end
end )

