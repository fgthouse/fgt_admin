-- This file holds functions that should only be used internally by fgtadmin or stock plugins.
-- These functions can be used by third party plugins but care must be taken when doing so.

-- Find player[s] by steam name. Returns a single user entity or a table of multiple matches
function fgtadmin:FindPlayersByName( name, oneTarget ) -- string, boolean
	local matchPly = {}

	for _, ply in pairs( player.GetAll() ) do
		if string.find( string.lower( ply:Nick() ), string.lower( name ) ) then
			if firstResult then
				return ply
			end

			table.insert( matchPly, ply )
		end
	end

	if #matchPly < 1 then
		return nil
	elseif oneTarget and (#matchPly > 1) then 	-- Multiple players found with similar name. (Requested one user.)
		return -1 				-- Plugins will need to know that -1 is multiple players found.
	elseif oneTarget and (#matchPly == 1) then	-- We only want one player and only found one. Return the player object.
		return matchPly[1]
	else						-- Return the table of players that matched.
		return matchPly
	end
end

-- Returns the steamid of the account that owns garrysmod. nil if the targetPlayer isn't using family sharing.
-- https://developer.valvesoftware.com/wiki/Steam_Web_API#IsPlayingSharedGame_.28v0001.29
function fgtadmin:GetGameOwnersSteamID( targetPlayer ) -- player
	-- TODO
end

-- Checks if the given player is banned and if so kick them from the server.
function fgtadmin:KickIfBanned( joiningPlayer ) -- player
	if not joiningPlayer or not joiningPlayer:IsValid() then -- Just in case PlayerAuthed or PlayerInitialSpawn messes up.
		self:Print( "KickIfBanned was called with an invalid player object!" )
		return
	end

	local joiningSteamId = joiningPlayer:SteamID()
	local bannedPlayer = self.bans[ joiningSteamId ] -- self.bans is an associative table. The key is the steamid and the value is a table of: name, reason, bandate, expirationdate.

	if bannedPlayer then -- Check if this player is in our ban table.
		if bannedPlayer["expirationdate"] == 0 then
			joiningPlayer:Kick( "You are permanently banned from this server! Reason: " .. bannedPlayer["reason"] )
			return

		elseif os.time() > bannedPlayer["expirationdate"] then -- This player has waited out their ban.
			self.bans[ joiningSteamId ] = nil -- Remove this players ban.
			return

		else
			local banDuration =  (bannedPlayer["expirationdate"] - os.time()) / 60
			local durationTable = self:ParseMinutesToString( banDuration, 1 ) -- Get the ban duration in one unit. Rounding up.
			local durationString = "Banned: " .. (durationTable[1] and (durationTable[1] .. " remaining.") or "a while")
			local banString = string.format( "%-47.47s%s", durationString, "Reason: " .. bannedPlayer["reason"] )

			joiningPlayer:Kick( banString )
			return
		end
	end
end

-- Will return the reference to fgtadmin.ranks.*
function fgtadmin:GetRankTable( rankName ) -- string
	for _, rank in pairs(self.ranks) do
		if rank.name == rankName then
			return rank
		end
	end

	return nil
end

-- Return a table of each priv, follow inheritance from power.
function fgtadmin:GetRankPrivileges( playerRank ) -- string
	local returnPrivs = {}
	local rankTable = self:GetRankTable( playerRank )
	if not type(rankTable) == "table" then
		self:Print( "There is no " .. playerRank .. " rank!" )
		return
	end

	table.Add( returnPrivs, rankTable.baseprivs ) -- Add the initial privs of this rank.
	table.Add( returnPrivs, rankTable.grantedprivs ) -- Add the manually granted privs.
	for _, rank in pairs(self.ranks) do
		if rank.power < rankTable.power then -- Add the privs of all ranks below this ranks power level. THIS WILL NOT ADD SAME POWER LEVEL PRIVIVILEGES!
			table.Add( returnPrivs, rank.baseprivs )
			table.Add( returnPrivs, rank.grantedprivs ) -- Also grant all manual granted privs below.
		end
	end
	for _, priv in pairs(rankTable.deniedprivs) do
		table.RemoveByValue( returnPrivs, priv )
	end
	return returnPrivs
end

-- Returns a table of privileges that a player has. (DO NOT USE THIS FUNCTION IN PLUGINS, See PlayerHasPrivilege())
function fgtadmin:GetPlayerPrivileges( targetPlayer ) -- player
	local targetPlayersRank = self:GetRank( targetPlayer )
	local targetPlayersPrivileges = self:GetRankPrivileges( targetPlayersRank )
	local targetPlayersRankTable = self:GetRankTable( targetPlayersRank )

	if targetPlayer:EntIndex() == 0 then -- Console is granted all privileges from the owner rank.
		return targetPlayersPrivileges
	else
		local targetPlayersInfoTable = self.users[ targetPlayer:SteamID() ] -- Check if this player is in our users table. (Has special grant privileges.)
	end

	if #targetPlayersRankTable.grantedprivs > 0 then -- Add any rank granted privileges.
		table.Add( targetPlayersPrivileges, targetPlayersRankTable.grantedprivs )
	end

	if targetPlayersInfoTable then -- Add any user granted privileges.
		table.Add( targetPlayersPrivileges, targetPlayersInfoTable.grantedprivs )
	end

	return targetPlayersPrivileges
end
