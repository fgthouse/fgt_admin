
util.AddNetworkString( "FgtAdmin_ChatNotification" ) -- Precache our chatnotification net message.
util.AddNetworkString( "FgtAdmin_ConsolePrint" ) -- Precache our consoleprint net message.

-- Used internally to figure out which plugin is callling fgtadmin:Functions() incorrectly.
function fgtadmin:FindPluginBlame( isDoingWhat )
	local pluginName = "<unknown>"
	local scope = 1
	if isDoingWhat == nil then
		isDoingWhat = "not properly calling functions!"
	end

	while true do
		local info = debug.getinfo(scope)
		if not info then  -- We're at the end of the stack.
			break
		end
		if string.find( info.short_src, "fgt_plugins" ) then
			local pluginExplode = string.Explode( "/", info.short_src )
			pluginName = pluginExplode[ #pluginExplode ]
			break
		end
		scope = scope + 1
	end
	self:Print( "ERROR: Plugin " .. pluginName .. " is " .. isDoingWhat )
end

-- Print to console with a prefix.
function fgtadmin:Print( str )
	print( " [Fgt Admin] " .. str )
end

-- Returns a table to be chat.AddText( unpack( this ) ) on the client.
-- This function will format a string like "# has kicked # from the server. (Reason: #)" by replacing all #'s with the color then text then white color again.
function fgtadmin:FormatAction( formattedString, variablesToInsert ) -- string, table of tables { {"text", Color()}, {"text",Color()} }
	if not formattedString then
		self:FindPluginBlame( "calling FormatAction() without a formattedString" )
		return
	end

	local returnTable = {} -- The data to be unpacked and chat.AddText()
	local startPos = 1 -- Remembers where we left off from the last # replacement.
	local buildingString = "" -- Holds the text before #'s
	
	local gsubRet, counted = string.gsub( formattedString, "#", "#" )
	if counted ~= #variablesToInsert then
		self:FindPluginBlame( "is calling fgtadmin:FormatAction() with too many/few replacement markers!" )
		return {}
	end

	for _, variableData in pairs( variablesToInsert ) do -- Go through each # we need to replace.
		for c = startPos, #formattedString do -- Go through each character of the string building anything but # into buildingString.
			if formattedString[c] ~= "#" then -- Build the string
				buildingString = buildingString .. formattedString[c]
			else
				local lastAdd = returnTable[#returnTable] -- Remember what we added last to the returnTable. (This is to avoid multiple colors overwriting each other.)
				if not lastAdd or (type(lastAdd) == "string") then
					table.insert( returnTable, Color( 255, 255, 255 ) ) -- Insert white color.
				end

				table.insert( returnTable, buildingString ) -- Add the text prior to the #

				if type(variableData) == "string" then -- Adding a variable with no color. Default is white.
					table.insert( returnTable, variableData )
				else -- This variable needs a custom color.
					table.insert( returnTable, variableData[2] ) -- Add the color that this variable should be.
					table.insert( returnTable, variableData[1] ) -- Add the variable text.
				end
				buildingString = "" -- Reset the buildingString, any text prior to the #
				startPos = c + 1 -- Skip the # and remember where we left off.
				break -- Break back into searching were we left off for the next #
			end
		end
	end

	if startPos-1 ~= #formattedString then -- We have text to add after our last # was replaced.
		table.insert( returnTable, Color( 255, 255, 255 ) ) -- Make the text white.
		for c = startPos, #formattedString do
			buildingString = buildingString .. formattedString[c]
		end
		table.insert( returnTable, buildingString )
	end

	return returnTable -- Pass this clusterfuck of colors and strings to the client.
end

-- Unpack the table and print to console. No colors or any of that.
function fgtadmin:PrintActionToConsole( sender, tableContent ) -- entity/player, table
	local returnStr = ""

	for _, content in pairs(tableContent) do
		if type( content ) == "Player" then
			returnStr = returnStr .. content:Nick()
		elseif type( content ) == "Entity" then
			returnStr = returnStr .. ( (content:EntIndex() == 0) and "Console" or (content:GetClass() .. ":" .. content:EntIndex()) )
		elseif type( content ) == "string" then
			returnStr = returnStr .. content
		elseif type( content ) == "table" then
		else
			self:FindPluginBlame( "calling fgtadmin:Action() with an unknown type (" .. type(content) .. ") in the table!" )
		end
	end

	return returnStr
end

-- Print an admin action to everyone's chat or if isSilent then only >= power ranks/users chat and log the event. (!kick or @kick)
function fgtadmin:Action( sender, tableAddText, isSilent ) -- The table will be unpacked and chat.AddText() on the client. Sender is the admin that did action.
	if (type(sender) ~= "Entity") and (type(sender) ~= "Player") then
		self:FindPluginBlame( "Calling fgtadmin:Action() without the admins entity" )
		return
	end

	if type(tableAddText) == "string" then -- Send just the string to the client. Still needs to be a table to be unpacked.
		tableAddText = { tableAddText }
	end

	if type(tableAddText) ~= "table" then
		self:FindPluginBlame( "Calling fgtadmin:Action() with invalid arguments!" )
		return
	end

	self:Print( (isSilent and "(Silent) " or "") .. self:PrintActionToConsole( sender, tableAddText ) )
	net.Start( "FgtAdmin_ChatNotification" )
	net.WriteBit( isSilent and true or false )
	net.WriteTable( tableAddText ) -- client code is chat.AddText( unpack( net.ReadTable() ) ) It's up to the plugin to format the message.

	if isSilent then -- Send to admins >= calling admins power level. (@kick)
		local receivingAdmins = {}
		local adminPower = self:GetRankPower( ((sender:EntIndex() == 0) and "owner" or sender:GetFgtRank()) )
		for _, player in pairs( player.GetHumans() ) do
			local playerPower = self:GetRankPower( player:GetFgtRank() )
			if playerPower >= adminPower then -- The message is silent to anyone below the calling admins power level.
				table.insert( receivingAdmins, player ) -- Add this player to our receiving table
			end
		end
		net.Send( receivingAdmins )
		return
	end	
	net.Broadcast() -- Send to everyone (!kick)
end

-- Send a message to the user and the server console. Also logged.
function fgtadmin:Warn( receiver, text )
	if (type(receiver) == "string") or (receiver == nil) then
		self:FindPluginBlame( "calling fgtadmin:Warn() with no receiving player." )
		return
	end

	if receiver:EntIndex() == 0 then -- Print message to console.
		self:Print( "WARNING: " .. text )
	else
		receiver:ChatPrint( text ) -- Print to clients chat.
		self:Print( "-> [" .. receiver:Nick() .. "] " .. text )
	end
end

-- Send an error message to the users console.
function fgtadmin:Error( receiver, text ) -- player, string
	if (type(receiver) == "string") or (receiver == nil) then
		self:FindPluginBlame( "calling fgtadmin:Error() with no receiving player." )
		return
	end

	if receiver:EntIndex() == 0 then -- Print message to console.
		self:Print( text )
	else
		receiver:ChatPrint( text )
	end
end

-- Send a message to the users console.
function fgtadmin:CPrint( receiver, text ) -- player, string
	if (type(receiver) == "string") or (receiver == nil) then self:FindPluginBlame( "calling fgtadmin:CPrint() with no receiving player." ) return end

	if receiver:EntIndex() == 0 then -- Print message to console.
		self:Print( text )
	else
		net.Start( "FgtAdmin_ConsolePrint" )
			net.WriteString( text )
		net.Send( receiver )
	end
end
