
local playerMeta = FindMetaTable("Player")

-- Returns the players networked string for fgtadmin rank.
function playerMeta:SetFgtRank( rank ) -- string
	if fgtadmin:RankExists( rank ) then
		self:SetNWString( "FgtRank", rank )
		local rankPower = fgtadmin:GetRankPower( rank )
		if rankPower >= fgtadmin.ranks.superadmin.power then
			self:SetNWString( "UserGroup", "superadmin" )
		elseif rankPower >= fgtadmin.ranks.admin.power then
			self:SetNWString( "UserGroup", "admin" )
		else
			self:SetNWString( "UserGroup", "user" )
		end
	else
		fgtadmin:FindPluginBlame( "calling SetFgtRank() with a non-existant rank, " .. rank .. "!" )
		self:SetNWString( "FgtRank", "guest" )
		self:SetNWString( "UserGroup", "user" )
	end
end

-- Returns the players fgtadmin rank (string)
function playerMeta:GetFgtRank()
	local vanillaGroup = self:GetNWString( "UserGroup" )
	local fgtRank = self:GetNWString( "FgtRank" )

	if fgtRank ~= "" then
		return fgtRank
	elseif (vanillaGroup == "superadmin") or (vanillaGroup == "admin") then
		return vanillaGroup
	else
		return "guest"
	end
end
