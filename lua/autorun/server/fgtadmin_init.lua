print("[ Loading Fgt Administration ... ]")

fgtadmin 	 = {}
fgtadmin.ranks   = {} -- { ["name"] = str, ["title"] = "Rank Name", ["baseprivs"] = table, ["grantedprivs"] = table, ["deniedprivs"] = table, ["power"] = integer) }
fgtadmin.users   = {} -- ["steamid"]{ ["name"] = str, ["rank"] = str, ["grantedprivs"] = table, ["deniedprivs"] = table } TODO: power modifier?
fgtadmin.bans    = {} -- ["steamid"]{ ["name"] = str, ["reason"] = str, ["bandate"] = int (epoch), ["expirationdate"] = int (epoch) }
fgtadmin.plugins = {} -- {name, desc, commands, helptext, priv, baserank}

local files, dirs = file.Find( "fgt_admin/*", "LUA" ) -- Load all the framework files.
for _, framework in pairs(files) do
	include( "fgt_admin/" .. framework )
end
fgtadmin:Print( "Framework Loaded!" )

fgtadmin:LoadRanks()
fgtadmin:LoadUsers()
fgtadmin:LoadBans()
fgtadmin:LoadPlugins()

-- Recreate the players and ranks data files. THIS WILL OVERWRITE EVERYTHING!
local function recreateDataFiles( ply, cmd, args ) -- player, string, table of arguments
	if (ply:EntIndex() == 0) or (ply:IsListenServerHost()) then
		fgtadmin.ranks = {}
		fgtadmin.users = {}

		file.Delete("fgtadmin_ranks.txt")
		file.Delete("fgtadmin_users.txt")

		fgtadmin:LoadRanks()
		fgtadmin:LoadUsers()
	end
end

-- Checks for the plugin that supports the requested command[s] and then excutes its Main() with the player and args.
function fgtAdminCommand( ply, cmd, args ) -- player, string, table of arguments
	if #args < 1 then
		fgtadmin:Error( ply, "Please give a command to run." )
		return
	end

	for _, plugin in pairs( fgtadmin.plugins ) do
		if table.HasValue( plugin.commands, args[1] ) then
			if cmd == "fa" then
				plugin:Main( ply, args, false, false ) -- Main( player, arguments, isSilent, calledByChat )
			elseif cmd == "fas" then
				plugin:Main( ply, args, true, false ) -- Call the plugin silently.
			else
				fgtadmin:Warn( ply, "How did you even manage this?" )
			end

			return
		end
	end

	fgtadmin:Error( ply, "There is no " .. args[1] .. " command!" )
end

--TODO: refine debug commands
concommand.Add( "fgtadmin_printranks", function( p ) if p:EntIndex() == 0 then PrintTable( fgtadmin.ranks ) end end )
concommand.Add( "fgtadmin_printusers", function( p ) if p:EntIndex() == 0 then PrintTable( fgtadmin.users ) end end )
concommand.Add( "fgtadmin_recreate", recreateDataFiles )
concommand.Add( "fa", fgtAdminCommand )
concommand.Add( "fas", fgtAdminCommand )

print("[    Done Loading Fgt Admin!     ]")
