
--[[ baseprivs are for inheritance. They also are given to base ranks on startup by plugins choice.
     If you need to deny a rank of a base priv then add it to the deniedprivs table.
     But remember all ranks above the denied rank will still inherit the privilege. ]]

fgtadmin.ranks = {}

fgtadmin.ranks.owner = {}
fgtadmin.ranks.owner.name = "owner"
fgtadmin.ranks.owner.title = "Owner"
fgtadmin.ranks.owner.baseprivs = {}
fgtadmin.ranks.owner.grantedprivs = {}
fgtadmin.ranks.owner.deniedprivs = {}
fgtadmin.ranks.owner.power = 100

fgtadmin.ranks.superadmin = {}
fgtadmin.ranks.superadmin.name = "superadmin"
fgtadmin.ranks.superadmin.title = "Super Admin"
fgtadmin.ranks.superadmin.baseprivs = {}
fgtadmin.ranks.superadmin.grantedprivs = {}
fgtadmin.ranks.superadmin.deniedprivs = {}
fgtadmin.ranks.superadmin.power = 80

fgtadmin.ranks.admin = {}
fgtadmin.ranks.admin.name = "admin"
fgtadmin.ranks.admin.title = "Admin"
fgtadmin.ranks.admin.baseprivs = {}
fgtadmin.ranks.admin.grantedprivs = {}
fgtadmin.ranks.admin.deniedprivs = {}
fgtadmin.ranks.admin.power = 60

fgtadmin.ranks.guest = {}
fgtadmin.ranks.guest.name = "guest"
fgtadmin.ranks.guest.title = "Guest"
fgtadmin.ranks.guest.baseprivs = {}
fgtadmin.ranks.guest.grantedprivs = {}
fgtadmin.ranks.guest.deniedprivs = {}
fgtadmin.ranks.guest.power = 10
